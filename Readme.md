Requirements:
------------

For setting up the master node, run  k8-master-installer.bash.
It requires ssh connection between the master and the worker nodes as cluster joining tokens are to be sent
to the worker nodes.

You need to have your ssh key-pair file(s) to be present on the master node.

You need to create a plain text file that contains all the worker node IPs.

_System requirements_

* Filesystem should be set to EXT4
* Minimum CPUs: 2
* Full connectivity among master and worker nodes


Usage information:
-----------------
**Master node**

 **Usage**: 

	./k8-master <ssh-key-path> <ssh-user-name> <worker-node-ip-list-file>

	<ssh-key-path> - Path of private key file. 
	<ssh-user-name> - User compatible with the private key.
	<worker-node-ip-list-file> - Path of file containing list of worker node IPs 
	
	Enter the IPs as: 
	<worker-node-ip>
	<worker-node-ip>
	<worker-node-ip>


**Worker node**

**Usage**:

	./k8-worker [wait]

	The "wait" parameter is optional. The installation on the worker node waits for joining the cluster until the token is sent by the Master node.



Known problems
--------------

For kubernetes to hosts containers, it requires one of the available types of CRI: Docker, CRI-O, containerd, frakti.
According to the docs ( https://kubernetes.io/docs/setup/cri/ ), using Docker as a CRI requires the storage-driver to be "overlay2".
For "overlay2" Centos must have d_type flag set during installation, otherwise docker server would not start.

**This will the error**:
	dockerd[13984]: Error starting daemon: error initializing graphdriver: overlay2: the backing xfs filesystem is formatted without d_type support

Details about how to set d_type can be found at - https://linuxer.pro/2017/03/what-is-d_type-and-why-docker-overlayfs-need-it/
If XFS is the filesystem of host OS, the d_type setting can be checked with - 

	  # xfs_info /
	
	  meta-data=/dev/vda1              isize=256    agcount=26, agsize=524224 blks
	           =                       sectsz=512   attr=2, projid32bit=1
	           =                       crc=0        finobt=0 spinodes=0
	  data     =                       bsize=4096   blocks=13106775, imaxpct=25
	           =                       sunit=0      swidth=0 blks
	  naming   =version 2              bsize=4096   ascii-ci=0 ftype=0
	  log      =internal               bsize=4096   blocks=2560, version=2
	           =                       sectsz=512   sunit=0 blks, lazy-count=1
	  realtime =none                   extsz=4096   blocks=0, rtextents=0
	 

The value ftype=0 on the 6th line shows d_type is not set. XFS by default installs with d_type unset.
This is not a problem if filesystem is set to **EXT4**.
