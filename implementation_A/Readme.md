Requirements:
------------

1) nc - Netcat package
2) Connectivity among master and worker nodes
3) Communication on port 1234 among master and worker nodes


How to run:
-----------

1) Clone the repository on all nodes in your cluster
2) On Master node,
	./k8-master <worker-node-ip>,<another-worker-node-ip>,...
   Run the k8-master script with the comma separated list of worker node IPs. This is req. for sending the joining token to worker nodes.
3) On Worker node,
	./k8-worker 



